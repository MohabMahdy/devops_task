#!/usr/bin/env bash

# Download docker images and create the required customized images.
docker-compose build

# Build and start our containers.
docker-compose up -d

# Create the default index for kibana. The name of the index is based on the name configured in logstash_conf/logstash.conf
echo "Waiting for all services to be up"

until curl -f -XPOST -H "Content-Type: application/json" -H "kbn-xsrf: anything" \
      http://localhost:5601/api/saved_objects/index-pattern/nginx \
      -d"{\"attributes\":{\"title\":\"nginx\",\"timeFieldName\":\"@timestamp\"}}" &> /dev/null
do
    sleep 1
done

curl -XPOST -H "Content-Type: application/json" -H "kbn-xsrf: anything" \
     "http://localhost:5601/api/kibana/settings/defaultIndex" \
      -d"{\"value\":\"nginx\"}" &> /dev/null

echo "The default index of kibana is set to nginx"
