# Description:
In this project, a web server is built using NGINX with caching and compression enabled. The NGINX logs will be sent to an Elasticsearch stack.A Logstash instance will be used to parse the log before it is passed to an Elasticsearch instance.A Kibana instance to provide the dashboard to view this logs.
This setup is achieved by using Docker 3 containers on an Ubuntu 16 Server.
'setup.sh' is used to build the customized docker images, run 'docker-compose' and create kibana's default index.


# NGINX caching and compression verification:
Regarding the caching and compression in NGINX, there effect can be verified using the below commands(sample output is provide):

-- with compression:  
** curl -H "Accept-Encoding: gzip" -I http://localhost  
HTTP/1.1 200 OK  
Server: nginx/1.15.3  
Date: Mon, 10 Sep 2018 18:44:10 GMT  
Content-Type: text/html  
Last-Modified: Sun, 09 Sep 2018 14:12:06 GMT  
Connection: keep-alive  
Vary: Accept-Encoding  
ETag: W/"5b952a36-44e4"  
Content-Encoding: gzip  

-- without compression:  
** curl -H "Accept-Encoding: gzip" -I http://localhost  
HTTP/1.1 200 OK  
Server: nginx/1.15.3  
Date: Mon, 10 Sep 2018 18:47:00 GMT  
Content-Type: text/html  
Content-Length: 17636  
Last-Modified: Sun, 09 Sep 2018 14:12:06 GMT  
Connection: keep-alive  
ETag: "5b952a36-44e4"  
Accept-Ranges: bytes  

-- with caching  
** curl -X GET -I http://localhost/style.css  
HTTP/1.1 200 OK  
Server: nginx/1.15.3  
Date: Mon, 10 Sep 2018 20:34:58 GMT  
Content-Type: text/css  
Content-Length: 2877  
Last-Modified: Mon, 10 Sep 2018 18:32:20 GMT  
Connection: keep-alive  
Vary: Accept-Encoding  
ETag: "5b96b8b4-b3d"  
Expires: Mon, 17 Sep 2018 20:34:58 GMT  
Cache-Control: max-age=604800  
Cache-Control: public  
Accept-Ranges: bytes  

-- without caching  
** curl -X GET -I http://localhost/style.css  
HTTP/1.1 200 OK  
Server: nginx/1.15.3  
Date: Mon, 10 Sep 2018 20:39:55 GMT  
Content-Type: text/css  
Content-Length: 2877  
Last-Modified: Mon, 10 Sep 2018 18:32:20 GMT  
Connection: keep-alive  
Vary: Accept-Encoding  
ETag: "5b96b8b4-b3d"  
Accept-Ranges: bytes  


# Dependencies:
    - docker  
    - docker-compose  


# How to use:
    - Clone the repository to the required machince.  
    - Make sure that the user you use for the following configuration has privilege to run docker-compose  
    - run "./setup.sh"  
    - The web server can be accessed from 'http://localhost'
    - The dashboard for log viewing can accessed from 'http://localhost:5601'
